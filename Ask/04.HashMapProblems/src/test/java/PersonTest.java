import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static org.junit.Assert.*;

public class PersonTest {
    @Test
    public void testHashMap() throws Exception {

        Map<Person, Integer> personMap = new HashMap<>();
        Iterator<Map.Entry<Person, Integer>> mapIter = personMap.entrySet().iterator();


        Person p1 = new Person(1, "x");
        Person p2 = new Person(3, "b");
        Person p3 = new Person(2, "c");
        Person p0 = new Person(2, "b");


        personMap.put(p0, 0);
        personMap.put(p1, 1);
        personMap.put(p2, 2);
        personMap.put(p3, 3);


        p1.setName("sss");


        //print
        System.out.println("print");
        mapIter = personMap.entrySet().iterator();
        while (mapIter.hasNext()) {
            System.out.println(mapIter.next());
        }

        //System.out.println(personMap.containsKey(p1)); // == false, clar de ce


        Person sss = new Person(1, "sss");
        Assert.assertEquals("Equal",p1,sss);
        System.out.println(personMap.containsKey(sss));  //??
        System.out.println(personMap.get(new Person(1, "sss")));  //??

//        assert (personMap.containsKey(new Person(1, "sss")));



    }



}