class Person implements Comparable<Person>{
    int age;
    String name;

    @Override
    public String toString() {
        return getName() + " " + getAge() + "\n---";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (age != person.age) return false;
        return name != null ? name.equals(person.name) : person.name == null;
    }

    @Override
    public int hashCode() {
        int result = age;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    public Person(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int compareTo(Person o) {
        if (this.age > (o.getAge())){
            return 1;
        }
        else if (this.age < (o.getAge())){
            return -1;
        }
        else return 0;
    }
}