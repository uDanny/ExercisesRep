import org.junit.Test;

import java.lang.reflect.Array;
import java.util.*;

import static org.junit.Assert.*;

public class PersonTest {


    public Set<Person> testTreeSet() throws Exception {
        Set<Person> personList = new TreeSet<Person>();


        Person p0 = new Person(0, "d");
        Person p1 = new Person(1, "a");
        Person p2 = new Person(2, "b");
        Person p3 = new Person(3, "c");

        personList.add(p3);
        personList.add(p2);
        personList.add(p0);
        personList.add(p1);

        System.out.println("Afisarea prin treeSet");
        for (Person person : personList){
            System.out.println(person.toString());
        }
        return personList;
    }

    public Queue<Person> testPriorityQueue() throws Exception {
        Queue<Person> priorityQueue = new PriorityQueue<Person>();


        Person p0 = new Person(0, "d");
        Person p1 = new Person(1, "a");
        Person p2 = new Person(2, "b");
        Person p3 = new Person(3, "c");

        priorityQueue.add(p3);
        priorityQueue.add(p2);
        priorityQueue.add(p0);
        priorityQueue.add(p1);

        System.out.println("Afisarea prin treeSet");
        for (Person person : priorityQueue){
            System.out.println(person.toString());
        }
        return priorityQueue;
    }

    @Test
    public void checkEquality() throws Exception {
        Object[] a  =  testTreeSet().toArray();
        Object[] b  =  testPriorityQueue().toArray();
        assertArrayEquals(a,b);

    }
}