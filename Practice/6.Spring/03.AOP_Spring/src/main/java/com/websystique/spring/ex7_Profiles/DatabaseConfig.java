package com.websystique.spring.ex7_Profiles;

import javax.sql.DataSource;

public interface DatabaseConfig {

    DataSource createDataSource();

}
