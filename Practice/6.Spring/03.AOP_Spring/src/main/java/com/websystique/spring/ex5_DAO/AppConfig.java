package com.websystique.spring.ex5_DAO;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.websystique.spring.ex5_DAO")
public class AppConfig {

}