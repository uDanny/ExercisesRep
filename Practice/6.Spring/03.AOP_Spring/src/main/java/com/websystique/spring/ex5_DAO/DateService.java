package com.websystique.spring.ex5_DAO;

import org.joda.time.LocalDate;

public interface DateService {

    LocalDate getNextAssessmentDate();
}