package com.websystique.spring.ex5_DAO;

import org.joda.time.LocalDate;
import org.springframework.stereotype.Service;

@Service("dateService")
public class DateServiceImpl implements DateService{

    @Override
    public LocalDate getNextAssessmentDate() {
        return new LocalDate(2015,10,10);
    }
}
