package com.websystique.spring.ex2_Constr_Setter.prinConstructor;

public class Communication2 {

        private Encryption encryption;

        /*
         * DI via Constructor Injection
         */
        public Communication2(Encryption encryption){
            this.encryption = encryption;
        }


        public void communicate(){
            encryption.encryptData();
        }


}
