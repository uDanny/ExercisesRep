package com.websystique.spring.ex5_DAO;

public interface EmployeeService {

    void registerEmployee(Employee employee);
}
