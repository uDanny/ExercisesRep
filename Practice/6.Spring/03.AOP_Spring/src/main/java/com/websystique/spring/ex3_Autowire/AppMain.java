package com.websystique.spring.ex3_Autowire;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppMain {
    public static void main(String args[]){
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("configSpringForEx2.xml");

        //autowire=byName
        Application application = (Application)context.getBean("application");
        System.out.println("Application Details : "+application);
    }
}
