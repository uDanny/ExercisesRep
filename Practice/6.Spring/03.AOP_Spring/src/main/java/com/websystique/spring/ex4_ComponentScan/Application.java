package com.websystique.spring.ex4_ComponentScan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("application")
public class Application {
    //@Resource(name="applicationUser") //@Resource == @Autowired, doar ca avem posibilitatea de a indica numele
    @Autowired(required = false)
    @Qualifier("applicationUser")
    private ApplicationUser user;

    @Override
    public String toString() {
        return "Application [user=" + user + "]";
    }
}
