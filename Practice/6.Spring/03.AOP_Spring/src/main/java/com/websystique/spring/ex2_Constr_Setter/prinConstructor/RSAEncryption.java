package com.websystique.spring.ex2_Constr_Setter.prinConstructor;

public class RSAEncryption implements Encryption {
    @Override
    public void encryptData() {
        System.out.println("Encrypting data using RSA Encryption");
    }
}
