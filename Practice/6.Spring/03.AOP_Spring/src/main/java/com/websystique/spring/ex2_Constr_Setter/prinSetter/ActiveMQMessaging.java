package com.websystique.spring.ex2_Constr_Setter.prinSetter;

public class ActiveMQMessaging implements Messaging{

    public void sendMessage() {
        System.out.println("Sending Message via Active MQ");
    }

}
