package com.websystique.spring.ex5_DAO;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppMain {
    public static void main(String[] args) {
        //AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class); // manual, prin config java file

        //automat scan, fara config java file
        AnnotationConfigApplicationContext  context = new AnnotationConfigApplicationContext();
        context.scan("com.websystique.spring.ex5");
        context.refresh();

        EmployeeService service = (EmployeeService) context.getBean("employeeService");

        /*
         * Register employee using service
         */
        Employee employee = new Employee();
        employee.setName("Danny Theys");
        service.registerEmployee(employee);

        context.close();
    }
}
