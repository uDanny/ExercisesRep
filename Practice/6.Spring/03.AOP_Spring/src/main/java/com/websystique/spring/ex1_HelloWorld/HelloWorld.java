package com.websystique.spring.ex1_HelloWorld;

public interface HelloWorld {
    void sayHello(String name);

}
