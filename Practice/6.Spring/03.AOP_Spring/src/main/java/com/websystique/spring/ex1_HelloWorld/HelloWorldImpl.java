package com.websystique.spring.ex1_HelloWorld;

public class HelloWorldImpl implements HelloWorld{
    @Override
    public void sayHello(String name) {
        System.out.println("Hello "+name);
    }
}
