package com.websystique.spring.ex7_Profiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages = "com.websystique.spring.ex7_Profiles")
public class AppConfig {
//this dataSource bean can be injected with
// different beans on different environment (MySQL dataSource on Development & ORACLE for Production e.g.).
    @Autowired
    public DataSource dataSource;


}
