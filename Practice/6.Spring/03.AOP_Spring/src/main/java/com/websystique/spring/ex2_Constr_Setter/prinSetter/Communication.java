package com.websystique.spring.ex2_Constr_Setter.prinSetter;


public class Communication {
    private Messaging messaging;

    /*
    * DI via Setter
    */
    public void setMessaging(Messaging messaging){
        this.messaging = messaging;
    }

    public void communicate(){
        messaging.sendMessage();
    }
}
