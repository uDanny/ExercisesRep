package com.websystique.spring.ex2_Constr_Setter.prinSetter;

import com.websystique.spring.ex2_Constr_Setter.prinConstructor.Communication2;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppMain {
    public static void main(String[] args) {
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("configSpringForEx2.xml");
        Communication2 app = (Communication2)context.getBean("communication2");
        app.communicate();
    }
}
