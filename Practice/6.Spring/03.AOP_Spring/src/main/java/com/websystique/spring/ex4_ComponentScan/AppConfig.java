package com.websystique.spring.ex4_ComponentScan;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.websystique.spring.ex4_ComponentScan")
public class AppConfig {
}
