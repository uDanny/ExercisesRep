package com.websystique.spring.ex7_Profiles;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppMain {
    public static void main(String args[]){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        //Sets the active profiles
        //context.getEnvironment().setActiveProfiles("Development"); // sau @ActiveProfile in Tests
        context.getEnvironment().setActiveProfiles("Production");
        //Scans the mentioned package[s] and register all the @Component available to Spring
        context.scan("com.websystique.spring.ex7");
        context.refresh();
        context.close();
    }

}