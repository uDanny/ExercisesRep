import com.pluralsight.model.Customer;
import com.pluralsight.service.CustomerService;
import com.pluralsight.service.CustomerServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
    public static void main(String[] args) {
        //CustomerService service = new CustomerServiceImpl();
            //XML:
//        ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
//        CustomerService service = appContext.getBean("customerService", CustomerService.class);
//
//        System.out.println(service.findAll().get(0).getFirstname());
        //JAVA:
        ApplicationContext appContext = new AnnotationConfigApplicationContext(Application.class);

        CustomerService service = appContext.getBean("customerService", CustomerService.class);

        System.out.println(service.findAll().get(0).getFirstname());
    }
}
