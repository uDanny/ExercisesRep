import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.PrintStream;

public class KnightMain {
    public static void main(String[] args) {
        //Java old
        Quest quest = new SlayDragonQuest(System.out);
        Knight k = new BraveKnight(quest);
        k.embarkOnQuest();

        //alternativa mai abstracta
        //Spring
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext(
                        "springConfig.xml");
        Knight knight = context.getBean(Knight.class);
        knight.embarkOnQuest();
        context.close();
    }
}
