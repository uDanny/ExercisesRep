public class BraveKnight implements Knight{
    private Quest quest;
    //sa adaugam la BraveKnight un Ministrel ar fi gresit, ei ar trebui sa existe paralel, fara sa depinda unul de altul
    public BraveKnight(Quest quest) {
        this.quest = quest;
    }
    public void embarkOnQuest() {
        quest.embark();
    }
}