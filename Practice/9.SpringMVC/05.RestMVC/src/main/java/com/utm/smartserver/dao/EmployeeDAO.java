package com.utm.smartserver.dao;


import com.utm.smartserver.model.Employee;

import java.util.List;

public interface EmployeeDAO {

    void saveEmployee(Employee employee);

    List<Employee> findAllEmployees();

    void deleteEmployeeById(int ssn);

    Employee findById(int ssn);

    void updateEmployee(Employee employee);
}
