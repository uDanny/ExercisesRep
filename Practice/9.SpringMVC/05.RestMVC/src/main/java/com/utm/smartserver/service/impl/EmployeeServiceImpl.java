package com.utm.smartserver.service.impl;

import com.utm.smartserver.dao.EmployeeDAO;
import com.utm.smartserver.dao.impl.EmployeeDAOImpl;
import com.utm.smartserver.model.Employee;
import com.utm.smartserver.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("employeeService")
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeDAO employeeDAO;

    public Employee findById(int id) {
        Employee employee = employeeDAO.findById(id);
        return employee;
    }

    public void saveEmployee(Employee employee) {
        employeeDAO.saveEmployee(employee);
    }

    public void updateEmployee(Employee employee) {
        employeeDAO.updateEmployee(employee);
    }

    public void deleteEmployeeById(int id) {
        employeeDAO.deleteEmployeeById(id);
    }

    public List<Employee> findAllEmployees() {
        List<Employee> allEmployees = employeeDAO.findAllEmployees();
        return allEmployees;
    }
}
