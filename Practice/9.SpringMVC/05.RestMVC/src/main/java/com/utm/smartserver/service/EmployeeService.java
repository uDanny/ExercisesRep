package com.utm.smartserver.service;

import com.utm.smartserver.model.Employee;
import com.utm.smartserver.model.User;

import java.util.List;


public interface EmployeeService {

    Employee findById(int id);

    void saveEmployee(Employee employee);

    void updateEmployee(Employee employee);

    void deleteEmployeeById(int id);

    List<Employee> findAllEmployees();

}
