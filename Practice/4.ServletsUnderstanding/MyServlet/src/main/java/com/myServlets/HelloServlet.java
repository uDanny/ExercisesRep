package com.myServlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "HelloServlet") //urlPatterns = {"/salut"} urlPatterns - obtional, astfel indicam link de la root
public class HelloServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
//        PrintWriter out = response.getWriter();
//        out.println("<h1> sallut </h1><p> " + request.getParameter("name") + "</p>");
//
//
//        out.flush();

        String shared = request.getParameter("name") + " trecut prin Servlet";

        request.setAttribute("sharedParam", shared); // add to request
        request.getSession().setAttribute("sharedParam", shared); // add to session
        this.getServletConfig().getServletContext().setAttribute("sharedParam", shared); // add to application context

        //V1: lucreaza doar prin sesiune
        //response.sendRedirect("/hello.jsp");

        //V2: lucreaza cu toate
        request.getRequestDispatcher("/hello.jsp").forward(request,response);

        //V3: lucreaza cu toate
//        RequestDispatcher rd = getServletContext().getRequestDispatcher("/hello.jsp");
//        rd.forward(request, response);
    }
}
