class MyOuter {
    private int x = 7;
    public void makeInner() {
        MyInner in = new MyInner();
        in.seeOuter();

    }
    class MyInner {
        private int y = 8;
        public void seeOuter() {
            System.out.println("Outer x is " + x);
            System.out.println("Inner class ref is " + this);
            System.out.println("Outer class ref is " + MyOuter.this);
        }



    }// close inner class definition

    void doClass() {
        MyOuter myOuter = new MyOuter();
        class MyInner2 {
            public void seeOuter() {
                System.out.println("Outer x is " + x);
            } // close inner class method
        } // close inner class definition
        MyInner2 mi = new MyInner2(); // This line must come
        // after the class
        mi.seeOuter();
    }
} // close outer class