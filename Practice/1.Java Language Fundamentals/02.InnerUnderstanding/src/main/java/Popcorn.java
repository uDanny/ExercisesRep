class Popcorn {
    public void pop() {
        System.out.println("popcorn");
    }
}
class Food {
    void anonymusMethod(){
        Popcorn p = new Popcorn() {
            public void pop() {
                System.out.println("anonymous popcorn");
            }
        };
        p.pop();
    }

}