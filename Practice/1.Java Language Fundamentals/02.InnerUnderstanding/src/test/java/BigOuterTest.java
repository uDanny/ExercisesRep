import org.junit.Test;

import static org.junit.Assert.*;

public class BigOuterTest {
    @Test
    public void nestedTest() throws Exception {
        BigOuter.Nest n = new BigOuter.Nest(); // both class names
        n.go();
        Broom.B2 b2 = new Broom.B2(); // access the enclosed class

        b2.goB2();
    }
}