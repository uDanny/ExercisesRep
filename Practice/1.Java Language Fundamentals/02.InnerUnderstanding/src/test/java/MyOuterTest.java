import org.junit.Test;

import static org.junit.Assert.*;

public class MyOuterTest {
    @Test
    public void classesTest() throws Exception {
        MyOuter mo = new MyOuter(); // gotta get an instance!
        MyOuter.MyInner inner = mo.new MyInner();
        inner.seeOuter();
        // sau asa
        MyOuter.MyInner inner2 = new MyOuter().new MyInner();
        inner2.seeOuter();
        //
        MyOuter.MyInner inner3 = new MyOuter().new MyInner();
        inner3.seeOuter();
        //
        mo.doClass();

    }
}