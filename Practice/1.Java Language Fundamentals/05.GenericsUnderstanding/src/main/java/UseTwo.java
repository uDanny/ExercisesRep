import java.util.ArrayList;
import java.util.List;

public class UseTwo<T, X, Z> {
    T one;
    X two;
    List<Z> list;
    UseTwo(T one, X two) {
        this.one = one;
        this.two = two;
    }
    T getT() { return one; }
    X getX() { return two; }


}