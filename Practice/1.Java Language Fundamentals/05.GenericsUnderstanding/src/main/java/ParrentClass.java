public class ParrentClass {
    public void out(){
        System.out.println("ParrentClass");
    }
}
class ChildClass1 extends ParrentClass {
    @Override
    public void out() {
        System.out.println("ChildClass1");
    }
}
class ChildOfChild extends ChildClass1 {
    @Override
    public void out() {
        System.out.println("ChildOfChild");
    }
}
class ChildClass2 extends ParrentClass {
    @Override
    public void out() {
        System.out.println("ChildClass2");
    }
}
