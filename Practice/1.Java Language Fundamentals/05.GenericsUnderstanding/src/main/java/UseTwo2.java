import java.util.ArrayList;
import java.util.List;

public class UseTwo2 {
    public <R> void makeArrayList(R r) { // take an object of an
        // unknown type and use a
        // "R" to represent the type
        List<R> list = new ArrayList<R>(); // now we can create the
        // list using "R"
        list.add(r);
    }
}
