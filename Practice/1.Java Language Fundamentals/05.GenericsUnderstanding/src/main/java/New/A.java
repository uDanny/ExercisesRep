package New;

public class A {
    public void out(){
        System.out.println("A");
    }
}

class B extends A {
    @Override
    public void out() {
        System.out.println("B");
    }
}

class C extends B{
    @Override
    public void out() {
        System.out.println("C");
    }
}

class D extends C{
    @Override
    public void out() {
        System.out.println("D");
    }
}
