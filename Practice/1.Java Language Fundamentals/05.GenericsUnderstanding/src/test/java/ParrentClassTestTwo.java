import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ParrentClassTestTwo {
    @Test
    public void defaultTest() throws Exception {
        List<ParrentClass> list = new ArrayList<>();
        list.add(new ChildClass1());
        list.add(new ChildClass2());
        list.add(new ParrentClass());

        List<? super ParrentClass> list2 = new ArrayList<>();
        list2.add(new ChildClass1());
        list2.add(new ParrentClass());

        List<? extends ParrentClass> list3 = new ArrayList<>();
        //list3.add(new C());

    }

    @Test
    public void parametersTest() throws Exception {
        List<ParrentClass> list = new ArrayList<>();
        //List<? extends ParrentClass> list = new ArrayList<>();
        list.add(new ChildClass1());
        list.add(new ChildClass1());
        list.add(new ParrentClass());
        extendAdd(list);

        //List<Number> numbers = new ArrayList<>();

        //numbers.add(BigDecimal.valueOf(1));


    }

    void extendAdd(List<ParrentClass> list) {
        list.add(new ChildClass2());
        list.add(new ChildClass1());
//        list.add(new ));


    }
}