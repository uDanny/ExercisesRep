package New;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ATest2<T,W extends C>{
    private List<T> t;
    private List<W> tExt;

    private List<A> list;
    @Before
    void b(){
        list = new ArrayList<>();
        list.add(new A());
    }

    @Test
    void testWildGenerics() {
        t = new ArrayList<>();
        t.add((T) new String("aa"));
        t.add((T) new A());

        System.out.println(t.toString());

    }

    @Test
    void testExt() {
        tExt = new ArrayList<>();
        tExt.add((W) new C());
        tExt.add((W) new D());
        System.out.println(tExt.toString());
    }

}