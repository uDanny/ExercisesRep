package New;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ATest {
    A a = new A();
    A b = new B();
    A c = new C();
    A d = new D();
    float i = 0;

    @Test
    void testWildcard() {

        List<A> aList = new ArrayList<>();
        aList.add(new A());
        aList.add(new C());

        List<?> bList = new ArrayList<>(aList);

        System.out.println(bList.toString());
    }

    @Test
    void testExtendWild() {

        List<C> aList = new ArrayList<>();
        aList.add(new C());
        List<D> aList2 = new ArrayList<>();
        aList.add(new D());

        extendWild(aList);
        extendWild(aList2);
    }

    public void extendWild(List<?extends C > list){
        for (Object o : list){
            ((A)o).out();
        }
    }



    @Test
    void testSuperWild() {
        List<A> aList = new ArrayList<>();
        aList.add(new A());
        List<B> aList2 = new ArrayList<>();
        aList.add(new B());
        List<C> aList3 = new ArrayList<>();
        aList.add(new C());


        superWild(aList);
        superWild(aList2);
        superWild(aList3);
    }

    public void superWild(List<?super C> list){
        for (Object o : list){
            ((A)o).out();
        }
    }
}