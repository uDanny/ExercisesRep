import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ParrentClassTest {
    @Test
    public void arrayTest() throws Exception {
        List myList = new ArrayList(); // can't declare a type
        myList.add("Fred"); // OK, it will hold Strings
        myList.add(new Person()); // and it will hold Persons too
        myList.add(new Integer(42)); // and Integers...

        List<Person> people = new ArrayList<Person>(myList);
    }

    @Test
    public void qTeste() throws Exception {
        List<Object> a = new ArrayList<>(); //<?> nu vrea
        java.lang.String s = new java.lang.String("ssassa");
        a.add(s);

        a.toString();
    }

    @Test
    public void inheritTest() throws Exception {
        addEmptyMethod(new ArrayList<ParrentClass>());
        addSuperMethod(new ArrayList<ParrentClass>());
        addExtendMethod(new ArrayList<ChildClass1>());
    }

    private void addEmptyMethod(ArrayList<?> parrentClasses) { //primeste orice Class-uri
        //parrentClasses.add(new ParrentClass()); //nu permite add-ul
    }

    public void addSuperMethod(ArrayList<? super ChildClass1> superMethod) { // primeste doar ChildClass-uri
        superMethod.add(new ChildClass1());
    }
    public void addExtendMethod(ArrayList<? extends ParrentClass> extendMethod) { //primeste ChildClass-uri
        //extendMethod.add(new ChildClass2()); //nu permite add-ul
    }
}