import org.junit.Test;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class J8 {
    List <Person> list;
    @Test
    public void name() throws Exception {
        list = new ArrayList<>();

        Person p1 = new Person(1, "x");
        Person p2 = new Person(3, "c");
        Person p3 = new Person(2, "b");
        Person p0 = new Person(2, "b");

        list.add(p0);
        list.add(p1);
        list.add(p2);
        list.add(p3);

        list.stream().forEach(person -> System.out.println(person.toString()));




        List<String> nameList = list.stream().map(Person::getName).collect(Collectors.toList());
        System.out.println("Parse numele : " + nameList.toString());

        List<String> nameLinkedList = list.stream().map(Person::getName).collect(Collectors.toCollection(LinkedList::new));
        System.out.println("Parse numele 2 (in LinkedList) : " + nameLinkedList.toString());

        int ageSum = list.stream().collect( Collectors.summingInt(Person::getAge));
        System.out.println("Parse suma anilor : " + ageSum);

        int ageP0Sum = (int) list.stream().filter(p0::equals).count();
        System.out.println("Parse suma anilor a obiectelor p0 : " + ageP0Sum);

        Person personP0 = list.stream().filter(p0::equals).findAny().get();
        System.out.println("Parsarea persoanei, care == p0 : " + personP0.toString());

        Person person3 = list.stream().skip(2).findFirst().get();
        System.out.println("Parsarea persoanei a 3-a : " + person3.toString());

        List<Person> personsName = list.stream().filter(person1 -> person1.getName().equals("b")).collect(Collectors.toList());
        System.out.println("Parsarea persoanelor cu numele \"b\" : \n" + personsName.toString());

        List<Person> noDup = list.stream().distinct().collect(Collectors.toList());
        System.out.println("Excluderea dublicatelor : \n" + noDup.toString());


        boolean checkElement = list.stream().anyMatch(new Person(3, "c") :: equals);
        System.out.println("Cautarea unui element age-3 si name-c : " + checkElement);

        List<Person> sortList = list.stream().sorted(new NameSort()).collect(Collectors.toList());
        System.out.println("Lista sortata alfabetic : \n" + sortList.toString());

        list.removeAll(Collections.singleton(p0));
        System.out.println("lista dupa Delete : \n" + list );



    }

}
