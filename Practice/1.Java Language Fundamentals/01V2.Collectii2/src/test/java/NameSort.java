import java.util.Comparator;

class NameSort implements Comparator<Person> {
    public int compare(Person o1, Person o2) {
        if (o1.equals(o2)) {
            return 0;
        }
        return o1.getName().toString().compareTo(o2.getName().toString());
    }
}