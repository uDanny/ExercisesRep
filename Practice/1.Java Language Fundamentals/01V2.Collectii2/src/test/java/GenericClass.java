import org.junit.Test;

public class GenericClass {
    @Test
    public void first() throws Exception {
        A a = new B();
        a.show();

    }
}
class A{
    public A() {
        System.out.println("a");
    }
    void show(){
        System.out.println("aa");
    }
}
class B extends A{
    public B() {
        System.out.println("b");
    }

    @Override
    void show() {
        System.out.println("bb");
    }
}