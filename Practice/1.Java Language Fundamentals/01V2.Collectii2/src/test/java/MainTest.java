import org.junit.Test;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class MainTest {
    List<Person> list;
    Set<Person> set;
    Map<Person, Integer> map;
    Iterator<Person> iterator;
    Iterator<Map.Entry<Person, Integer>> mapIter;

    @Test
    public void arrayListM() throws Exception {
        list = new ArrayList<Person>();


        Person p1 = new Person(1, "x");
        Person p2 = new Person(3, "c");
        Person p3 = new Person(2, "b");
        Person p0 = new Person(2, "b");

        list.add(p1);
        list.add(p2);
        list.add(0, p3);
        list.add(0, p0);


        NameSort nameSort = new NameSort();

        //sort byNum & byComparable
        Collections.sort(list);
        System.out.println("byNum ");
        //print
        show(list);

        //sort alph & byComparator
        Collections.sort(list, nameSort);
        System.out.println("byAlph ");
        //print
        show(list);

        //size
        System.out.println("Size : " + list.size());

        //contains
        if (list.contains(p2)) {
            System.out.println("contine p2");
        }
        if (p3.equals(p0)) {
            System.out.println("p1 == p0");
        }


        //get
        System.out.println("obiectul " + 3 + " este: " + list.get(0).toString());

        //remove
        list.remove(0);
        System.out.println("after remove: ");
        show(list);

        System.out.println("aA".hashCode());
        System.out.println("ab".hashCode());

        //search
        System.out.println("search");
        list.add(new Person(22, "m"));
        Collections.sort(list, nameSort);
        System.out.println(Collections.binarySearch(list, p2, nameSort));

        //show by iterator
        iterator = list.iterator();
        System.out.println("iterator");
        while (iterator.hasNext()) {
            System.out.println(iterator.next().name);
        }




    }

    public void show(List<Person> list) {
        for (Person p : list) {
            System.out.println(p.toString());
        }
    }

    @Test
    public void hashSetM() throws Exception {
        set = new TreeSet<Person>();
        Person p1 = new Person(1, "x");
        Person p2 = new Person(3, "b");
        Person p3 = new Person(2, "c");
        Person p0 = new Person(2, "b");
        set.add(p0);
        set.add(p1);
        set.add(p2);

        ///System.out.println(" == " + p3.equals(p0));
        System.out.println(" set " + set.add(p3));

        iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());

        }
        ///System.out.println("continte p3 : " + set.contains(p3));

        System.out.println("headset");
        Set<Person> temp = ((TreeSet) set).headSet(p2);
        iterator = temp.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());

        }


    }

    @Test
    public void treeMap() throws Exception {
        // creating maps
        TreeMap<Integer, String> treemap = new TreeMap<Integer, String>();
        SortedMap<Integer, String> treemapincl;

        // populating tree map
        treemap.put(2, "two");
        treemap.put(1, "one");
        treemap.put(3, "three");
        treemap.put(6, "six");
        treemap.put(5, "five");
        treemap.put(4, "four");

        System.out.println("submap");
        treemapincl = treemap.headMap(3);
        System.out.println("Sub map values: " + treemapincl);


    }


    @Test
    public void hashMap() throws Exception {
        map = new HashMap<Person, Integer>();
        Person p1 = new Person(1, "x");
        Person p2 = new Person(3, "b");
        Person p3 = new Person(2, "c");
        Person p0 = new Person(2, "b");

        map.put(p0, 0);
        map.put(p1, 1);
        map.put(p2, 2);
        map.put(p3, 3);


        p1.setName("sss");


        //print
        System.out.println("print");
        mapIter = map.entrySet().iterator();
        while (mapIter.hasNext()) {
            System.out.println(mapIter.next());
        }

        System.out.println(map.get(new Person(1, "sss")));//??

        ///
        //Integer result = map.entrySet().stream().filter(map     )

        //filter(map->(new Integer(2)).equals(map.getValue())).map(map->map.getValue())

        ///Person person  = map.entrySet().stream().filter(m -> "b".equals(m.getKey().getName())).map(m -> m.getValue()).collect(Collectors.joining())

    }

    @Test
    public void priority() throws Exception {
        Queue<Person> queue = new PriorityQueue<Person>();
        Iterator<Person> it = queue.iterator();
        Person p1 = new Person(1, "x");
        Person p2 = new Person(3, "c");
        Person p3 = new Person(2, "b");
        Person p0 = new Person(2, "b");

        queue.add(p0);
        queue.add(p1);
        queue.add(p2);
        queue.add(p3);

        System.out.println("contains " + queue.contains(new Person(3, "c")));


        queue.stream().forEach(person -> System.out.println(person.toString()));

//        for (Person p : queue)
//            System.out.println(p.toString());
//        }


    }


}
