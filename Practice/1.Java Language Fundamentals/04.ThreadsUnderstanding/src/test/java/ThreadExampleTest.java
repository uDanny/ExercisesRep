import org.junit.Test;

import static org.junit.Assert.*;

public class ThreadExampleTest {
    @Test
    public void testThread() throws Exception {
        ThreadExample threadExample = new ThreadExample();
        Thread thread1 = new Thread(threadExample);
        thread1.setName("one");

        Thread thread2 = new Thread(threadExample);
        thread2.setName("two");

        Thread thread3 = new Thread(threadExample);
        thread3.setName("three");

        thread1.setPriority(10);
        thread3.setPriority(1);


        thread1.start();
        thread2.start();
        thread3.start();

        //TODO: find out why it isn't work correctly, and needs sleep to perform operations
        //Thread.sleep(50000);

    }
}