public class Syncronizate {

    public static void main(String[] args) {
        SyncClass syncClass = new SyncClass();
        Thread t1 = new Thread(syncClass);
        t1.setName("A1");


        Thread t2 = new Thread(syncClass);
        t2.setName("A2");

        t1.start();

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t2.start();

        //DA, pot fi apelate metodele nonSyn, chiar dupa ce s-a inceput exec. sync
        //TODO: de aflat daca se blocheaza obiectul sau metoda syncronized???

    }
}
