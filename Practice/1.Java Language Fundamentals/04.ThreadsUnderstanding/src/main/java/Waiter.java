public class Waiter {
    public static void main(String[] args) {
        Notifyer notifyer = new Notifyer();
        Thread t1 = new Thread(notifyer);
        t1.start();
//        synchronized (t1){
//            try {
//                t1.wait();
//                System.out.println("end");
//
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
        //inlocuire la wait - care previne coruperea posibila de aruncarea notify inainte de a citi wait
        //exemplu corupere = badcode ( fara // safe improvements == badCode )
        try {
            Thread.sleep(5*60);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        synchronized (t1){
            try {
                while (!notifyer.isDone){ // safe improvements
                    t1.wait(); // la etapa cind wait, notify-ul deja a fost rulat, deci asteapta la infinit
                } // safe improvements
                System.out.println("end");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //TODO: check if my approach is right




    }
}
