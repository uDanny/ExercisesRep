public class InteractionClass {
}
class ThreadA {
 public static void main(String [] args) {
        ThreadB b = new ThreadB();
        b.start();
                synchronized(b) {
            try {
                System.out.println("Waiting for b to complete...");
                b.wait(); // in cazul acesta este echivalent cu un join
                 } catch (InterruptedException e) {}
             System.out.println("Total is: " + b.total);
             }
         }
 }

 class ThreadB extends Thread {
     int total;

     public void run() {
     synchronized(this) {
         for(int i=0;i<100;i++) {
             total += i;
             System.out.println(total);
             }
         notify();
         }
 }
 }