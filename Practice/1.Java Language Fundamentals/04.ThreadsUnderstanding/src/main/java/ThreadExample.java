import java.util.concurrent.TimeUnit;

public class ThreadExample implements Runnable {
    public void run() {
        for (int x = 1; x <= 25; x++) {
            System.out.println("Run by "
                    + Thread.currentThread().getName()
                    + ", x is " + x);
            try {
                Thread.sleep(5*60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

