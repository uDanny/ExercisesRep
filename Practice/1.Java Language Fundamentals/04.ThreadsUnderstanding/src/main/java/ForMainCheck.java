public class ForMainCheck {
    public static void main(String[] args) {
        ThreadExample threadExample = new ThreadExample();
        ThreadExample1 threadExample1 = new ThreadExample1();

        Thread thread1 = new Thread(threadExample);
        thread1.setName("one");

        Thread thread2 = new Thread(threadExample);
        thread2.setName("two");

        Thread thread3 = new Thread(threadExample1); // cu yield
        thread3.setName("three");

//        thread3.setPriority(10);
//        thread1.setPriority(1);

        thread1.start();

        try {
            thread1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread2.start();
        thread3.start();



    }
}
