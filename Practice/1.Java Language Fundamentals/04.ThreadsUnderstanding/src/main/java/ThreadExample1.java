import static java.lang.Thread.yield;

public class ThreadExample1 implements Runnable {
    public void run() {
        yield();

        for (int x = 1; x <= 25; x++) {
            System.out.println("Run by "
                    + Thread.currentThread().getName()
                    + ", x is " + x);
            try {
                Thread.sleep(5*60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
