public class SimpleWaiter {
    public static void main(String[] args) {
        SimpleNotifyer simpleNotifyer= new SimpleNotifyer();
        Thread t1 = new Thread(simpleNotifyer);
        t1.start();
        synchronized (t1){
            try {
                t1.wait();
                System.out.println("end");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
