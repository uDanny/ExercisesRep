public class SyncClass implements Runnable{
    int a = 0;
    public synchronized void aMethod() {

        for (int i = 0; i < 10; i++){
            try {
                Thread.sleep(100);
                a++;
                System.out.println( Thread.currentThread().getName() + " sync - " + a); // de ce sout by main?
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    ///
    public synchronized void bMethod() {
        for (int i = 0; i < 10; i++){
            try {
                Thread.sleep(100);
                a++;
                System.out.println( Thread.currentThread().getName() + " sync2 - " + a); // de ce sout by main?
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void aMethodNotSync() {
        //System.out.println("aa" + Thread.currentThread().getName());
        //bMethod();
        for (int i = 0; i < 10; i++){
            try {
                Thread.sleep(100);
                a++;
                System.out.println(  Thread.currentThread().getName() + " nonsync - " + a);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void run() {
        aMethodNotSync();
        aMethod();
    }
}
