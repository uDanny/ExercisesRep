package com.endava;//package com.endava;

import java.util.Comparator;
import java.util.PriorityQueue;

public class MainQueue {
    public static void main(String... args ){

        PriorityQueue<Person> personPriorityQueue=new PriorityQueue<>( new PriorityQueueComparator());
        personPriorityQueue.add(new Person("ion", 25));
        personPriorityQueue.add(new Person("vasile", 55));
        personPriorityQueue.add(new Person("sdadas", 35));


        for(Person s:personPriorityQueue){
            System.out.println(s.getName() + " " + s.getAge());
        }
    }
}
class PriorityQueueComparator implements Comparator<Person> {
    public int compare(Person p1, Person p2) {
        if (p1.getAge() < p2.getAge()) {
            return 1;
        }
        if (p1.getAge() > p2.getAge())  {
            return -1;
        }
            return 0;

    }
}
