package com.endava;

import java.util.ArrayList;
import java.util.List;

public class PersonsList {
    ArrayList <Person> personList;

    public PersonsList() {
        personList = new ArrayList<>();
    }
    public void addPerson(Person person){
        personList.add(person);
    }

    public Person getPerson(int i){
        return personList.get(i);
    }
    public Person getPersonByAge(int i){
        for (Person person1 : personList){
            if (person1.getAge() == i){
                return person1;
            }
        }
        return null;
    }

    public ArrayList<Person> getPersonListgetPersonList() {
        return personList;
    }

    public void setPersonList(ArrayList<Person> personList) {
        this.personList = personList;
    }
    public void delete(int i){
        personList.remove(i);
    }

    public ArrayList<Person> getPersonList() {
        return personList;
    }
}
