package com.endava;

import java.util.LinkedList;
import java.util.Vector;

public class PersonsVector {
    Vector<Person> personList;

    public PersonsVector(PersonsLinkedList personList) {
        this.personList = new Vector<>(personList.getPersonList());
    }

    public Vector<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(Vector<Person> personList) {
        this.personList = personList;
    }
    public void addPerson(Person person){
        personList.add(person);
    }
}
