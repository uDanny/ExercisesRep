package com.endava;

import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class MainSet {
    public static void main(String[] args) {
        //HashSet
        System.out.println("HashSet");
        HashSet<Person> hashSet = new HashSet<>();

        Person person1 = new Person("ion", 25);

        hashSet.add(person1);
        hashSet.add(new Person("vasile", 55));
        hashSet.add(new Person("fra", 35));

        hashSet.add(person1);


        for(Person person:hashSet){
            System.out.println(person.getName() + " " + person.getAge());
        }


        //LinkedHashSet
        System.out.println("\nLinkedHashSet");
        LinkedHashSet<Person> linkedHashSet = new LinkedHashSet<>(hashSet);
        for(Person person:linkedHashSet){
            System.out.println(person.getName() + " " + person.getAge());
        }


        //TreeSet
        System.out.println("\nTreeSet");
        TreeSet<Person> treeSet = new TreeSet<>(linkedHashSet);
        for(Person person:treeSet){
            System.out.println(person.getName() + " " + person.getAge());
        }

        //Sort
        System.out.println("\nSort");
        Comparator<Person> comp = (Person o1, Person o2) -> (o2.compareTo(o1));
        TreeSet<Person> ts = new TreeSet<>(comp);
        //ts = treeSet;
        ts.add(person1);
        ts.add(new Person("vasile", 55));
        ts.add(new Person("fra", 35));

        for(Person person:ts){
            System.out.println(person.getName() + " " + person.getAge());
        }



    }
}

