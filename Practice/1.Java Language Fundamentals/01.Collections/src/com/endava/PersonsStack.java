package com.endava;

import java.util.Stack;
import java.util.Vector;

public class PersonsStack {
    Stack <Person> personList;

    public PersonsStack(PersonsVector personList) {
        this.personList = new Stack<>();
        for (Person person1 : personList.getPersonList()){
            this.personList.push(person1);
        }
    }

    public Stack<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(Stack<Person> personList) {
        this.personList = personList;
    }
}
