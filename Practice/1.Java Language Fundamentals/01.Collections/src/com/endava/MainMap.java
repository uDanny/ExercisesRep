package com.endava;

import com.sun.javafx.collections.ObservableMapWrapper;

import java.util.*;

import static java.lang.Thread.sleep;

public class MainMap {
    public static void main(String[] args) throws InterruptedException {
        //hashtable
        System.out.println("hashtable");
        Hashtable<Integer, Person> hashtable = new Hashtable<>();
        Person person1 = new Person("dasgda", 37);
        Person person2 = new Person("vasile", 35);
        Person person3 = new Person("fra", 5);

        Thread t1 = new Thread(() -> {
            hashtable.put(1, person2);
        });
        Thread t2 = new Thread(() -> {

            hashtable.put(2, person3);
        });
        t1.start();
        t2.start();

        sleep(200);

        for (Map.Entry<Integer, Person> personEntry : hashtable.entrySet()) {
            System.out.println(personEntry.getKey() + " " + personEntry.getValue().getName() + " " + personEntry.getValue().getAge());
        }

        //HashMap
        System.out.println("\nHashMap");
        HashMap<Integer, Person> hashMap = new HashMap<>();
        hashMap.put(1, person2);
        hashMap.put(3, person3);
        hashMap.put(2, person2);

        for (Map.Entry<Integer, Person> mapEntry : hashMap.entrySet()) {
            System.out.println(mapEntry.getKey() + " " + mapEntry.getValue().getName() + " " + mapEntry.getValue().getAge());
        }

        //iterator
        System.out.println("\nIterator");
        Iterator<Map.Entry<Integer, Person>> itr = hashMap.entrySet().iterator();

        while (itr.hasNext()) {
            Map.Entry<Integer, Person> itr1 = itr.next();
            System.out.println(itr1.getKey() + " " + itr1.getValue().getName() + " " + itr1.getValue().getAge());
        }

        //LinkedHashMap
        System.out.println("\nLinkedHashMap");
        LinkedHashMap<Integer, Person> linkedHashMap = new LinkedHashMap<>(15, 0.75f, true  );
        linkedHashMap.put(3, person2);
        linkedHashMap.put(1, person3);
        linkedHashMap.put(2, person1);




        for (Map.Entry<Integer, Person> mapEntry : linkedHashMap.entrySet()) {
            System.out.println(mapEntry.getKey() + " " + mapEntry.getValue().getName() + " " + mapEntry.getValue().getAge());
        }


        //TreeMap
        System.out.println("\nTreeMap");
        TreeMap<Integer, Person> treeMap = new TreeMap<>();
        treeMap.put(1, person2);
        treeMap.put(3, person3);
        treeMap.put(2, person2);

        for (Map.Entry<Integer, Person> mapEntry : treeMap.entrySet()) {
            System.out.println(mapEntry.getKey() + " " + mapEntry.getValue().getName() + " " + mapEntry.getValue().getAge());
        }

        //custom sorted
        System.out.println("custom sort");
        TreeMap<Integer, Person> treeMap2 = new TreeMap<Integer, Person>(new comp(){});
        treeMap2.put(1, person2);
        treeMap2.put(3, person3);
        treeMap2.put(2, person2);


        for (Map.Entry<Integer, Person> mapEntry : treeMap2.entrySet()) {
            System.out.println(mapEntry.getKey() + " " + mapEntry.getValue().getName() + " " + mapEntry.getValue().getAge());
        }


    }
}

class comp implements Comparator<Integer>{

    @Override
    public int compare(Integer person, Integer t1) {
        if(person< t1){
            return -1;
        }else {
            return 1;
        }

    }
}
