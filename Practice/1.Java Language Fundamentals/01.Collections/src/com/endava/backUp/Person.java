package com.endava;

public class Person implements  Comparable<Person>{
    private String name;
    private int age;

    public Person() {
        name = new String();
        age = 0;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int compareTo(Person person) {
        if(equals(person)){ //check daca obiectele sunt egale, nu mai are sens de le comparat dupa vre-un parametru
            return 0;
        }
        if (this.age == person.getAge()){ return 0;}
        if (this.age > person.getAge()){ return 1;}
        if (this.age < person.getAge()){ return -1;}
        return 0;
    }
}
