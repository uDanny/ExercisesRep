package com.endava;

import java.util.*;

import static java.lang.Thread.sleep;

public class Main {

    public static String getRandomWord(int length) {
        Random random = new Random();
        StringBuilder word = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            word.append((char)('a' + random.nextInt(26)));
        }
        return word.toString();
    }
    private static void insertValues(PersonsList personsList){
        for (int i = 25; i < 35; i++){
            Person person = new Person();
            person.setName(getRandomWord(i));
            person.setAge(i);
            personsList.addPerson(person);
            personsList.addPerson(person);
        }
    }



    private static void getValues(PersonsList personsList){
        for (Person person1 : personsList.getPersonList()){
            System.out.println(person1.getName() + " " + person1.getAge());
        }
    }
    private static void getValues(PersonsLinkedList personsLinkedList){
        for (Person person1 : personsLinkedList.getPersonList()){
            System.out.println(person1.getName() + " " + person1.getAge());

        }
    }
    private static void getValues(PersonsVector personsVector){
        for (Person person1 : personsVector.getPersonList()){
            System.out.println(person1.getName() + " " + person1.getAge());

        }
    }

        public static void main(String[] args) throws InterruptedException {

        PersonsList personsList = new PersonsList();
        insertValues(personsList);
        getValues(personsList);


        System.out.println("/n");
        System.out.println("search by index : " + personsList.getPerson(0).getName());
        System.out.println("search by name : " + personsList.getPersonByAge(30).getName());

        personsList.delete(5);

        getValues(personsList);

        ///linkedList
            System.out.println("linkedList");
        PersonsLinkedList personsLinkedList = new PersonsLinkedList(personsList);

        personsLinkedList.popMethod();
         Person person1 = new Person("ion", 25);


        personsLinkedList.pushMethod(person1);
        getValues(personsLinkedList);


        //Vector
            System.out.println("vector");
            Person person2 = new Person("vasile", 35);
            Person person3 = new Person("fra", 5);
            PersonsVector personsVector = new PersonsVector(personsLinkedList);
            Thread t1 = new Thread(()->{
                personsVector.addPerson(person2);
            });
            Thread t2 = new Thread(()->{

                personsVector.addPerson(person3);
            });
            t1.start();
            t2.start();

            sleep(200);
            getValues(personsVector);

            //Stack
            System.out.println("Stack");
            PersonsStack personsStack = new PersonsStack(personsVector);
            System.out.println(personsStack.getPersonList().peek().getName());




















        }



}
