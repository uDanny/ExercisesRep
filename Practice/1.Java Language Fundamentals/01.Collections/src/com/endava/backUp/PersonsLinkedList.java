package com.endava;

import java.util.LinkedList;
import java.util.List;

public class PersonsLinkedList {
    LinkedList<Person> personList;

    public PersonsLinkedList() {
        personList = new LinkedList<>();
    }

    public PersonsLinkedList(PersonsList personsList) {
        this.personList = new LinkedList<>(personsList.getPersonList());
    }

    public LinkedList<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(LinkedList<Person> personList) {
        this.personList = personList;
    }

    public Person popMethod(){
        return personList.pop();
    }
    public void pushMethod(Person person){
        personList.push(person);
    }

}
