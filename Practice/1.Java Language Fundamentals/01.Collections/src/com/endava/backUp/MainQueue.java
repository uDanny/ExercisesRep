package com.endava;//package com.endava;
//
//import java.util.Comparator;
//import java.util.PriorityQueue;
//import java.util.function.Function;
//
//public class MainQueue {
//
//
//    public static void main(String[] args) {
//
//        PriorityQueueComparator strategy = new PriorityQueueComparator();
//        PriorityQueue<Person> personPriorityQueue = new PriorityQueue<>(strategy);
//
//
//        personPriorityQueue.add(new Person("ion", 25));
//        personPriorityQueue.add(new Person("vasile", 55));
//        personPriorityQueue.add(new Person("fra", 35));
//
//        for (Person person : personPriorityQueue){
//            System.out.println(person.getName() + " " + person.getAge());
//        }
//
//    }
//
//
//}
//
//class PriorityQueueComparator implements Comparator<Person> {
//    public int compare(Person s1, Person s2) {
//        if (s1.getAge() < s2.getAge()) {
//            return -1;
//        }
//        if (s1.getAge() > s2.getAge()) {
//            return 1;
//        }
//        return 0;
//    }
//}


import java.util.Comparator;
import java.util.PriorityQueue;

public class MainQueue {
    public static void main(String... args ){

        PriorityQueue<Person> personPriorityQueue=new PriorityQueue<>( new PriorityQueueComparator());
        personPriorityQueue.add(new Person("ion", 25));
        personPriorityQueue.add(new Person("vasile", 55));
        personPriorityQueue.add(new Person("fra", 35));

        for(Person s:personPriorityQueue){
            System.out.println(s.getName() + " " + s.getAge());
        }
    }
}
class PriorityQueueComparator implements Comparator<Person> {
    public int compare(Person p1, Person p2) {
        if (p1.getAge() > p2.getAge()) {
            return 1;
        }
        if (p1.getAge() < p2.getAge())  {
            return -1;
        }
            return 0;

    }
}


//public class PriorityQueueTest {
//    public static void main(String... args ){
//        PriorityQueueComparator pqc=new PriorityQueueComparator();
//        PriorityQueue<String> pq=new PriorityQueue<String>(5,pqc);
//        pq.add("ABC");
//        pq.add("BD");
//        pq.add("ABCD");
//        for(String s:pq){
//            System.out.println(s);
//        }
//    }
//}
//class PriorityQueueComparator implements Comparator<String>{
//    public int compare(String s1, String s2) {
//        if (s1.length() < s2.length()) {
//            return -1;
//        }
//        if (s1.length() > s2.length()) {
//            return 1;
//        }
//        return 0;
//    }
//}