package com.endava;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class MainTreeMapClass {
    private TreeMap<Integer, Person> treeMap;
    public Iterator iterator;

    public MainTreeMapClass() {
        treeMap = new TreeMap<>();
        Collection c = treeMap.values();
        this.iterator = c.iterator();
    }
    public void display(){
        while (iterator.hasNext()){
            Person pair = (Person) iterator.next();
            System.out.println(pair.getName() + " " + pair.getAge());
            System.out.printf("aa");
        }
    }
    public void add(Integer i, Person person){
        treeMap.put(i, person);
    }
}
class MainTreeMapClassRun {
    public static void main(String[] args) {
        MainTreeMapClass mainTreeMapClass = new MainTreeMapClass();
        Person person1 = new Person("Ion", 29);
        Person person2 = new Person("Vasile", 36);
        Person person3 = new Person("Eugen", 41);

        mainTreeMapClass.add(1, person1);
        mainTreeMapClass.add(3, person2);
        mainTreeMapClass.add(2, person3);

        mainTreeMapClass.display();
    }
}


