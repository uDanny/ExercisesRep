package com.endava;

import java.util.*;

public class MainTreeMapClass {
    private TreeMap<Integer, Person> treeMap;
    public Iterator<Map.Entry<Integer, Person>> iterator;

    public MainTreeMapClass(comparator comparator) {
        treeMap = new TreeMap<>(comparator);
        iterator = treeMap.entrySet().iterator();
    }
    public void refreshIterator(){
        iterator = treeMap.entrySet().iterator();
    }
    public void display(){
        refreshIterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, Person> itr1 = iterator.next();
            System.out.println(itr1.getKey() + " " + itr1.getValue().getName() + " " + itr1.getValue().getAge());
        }
    }
    public void add(Integer i, Person person){
        treeMap.put(i, person);
    }
}
class MainTreeMapClassRun {
    public static void main(String[] args) {
        MainTreeMapClass mainTreeMapClass = new MainTreeMapClass(new comparator());
        Person person1 = new Person("Ion", 29);
        Person person2 = new Person("Vasile", 36);
        Person person3 = new Person("Eugen", 41);

        mainTreeMapClass.add(1, person1);
        mainTreeMapClass.add(3, person2);
        mainTreeMapClass.add(2, person3);

        mainTreeMapClass.display();


    }
}
class comparator implements Comparator<Integer> {

    @Override
    public int compare(Integer person, Integer t1) {
        if(person> t1){
            return -1;
        }else {
            return 1;
        }

    }
}





