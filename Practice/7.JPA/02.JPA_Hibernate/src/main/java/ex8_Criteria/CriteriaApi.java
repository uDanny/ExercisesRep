package ex8_Criteria;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class CriteriaApi {
    public static void main(String[] args) {

        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "JavaHelps" );
        EntityManager entitymanager = emfactory.createEntityManager( );

        //introducerea datelor
        entitymanager.getTransaction( ).begin( );

        Employee employee1 = new Employee(401, "Gopal", 40000, "Technical Manager");
        Employee employee2 = new Employee(402, "Manisha", 40000, "Proof reader");
        Employee employee3 = new Employee(403, "Masthanvali", 35000, "Technical Writer");
        Employee employee4 = new Employee(404, "Satish", 30000, "Technical writer");
        Employee employee5 = new Employee(405, "Krishna", 30000, "Technical Writer");
        Employee employee6 = new Employee(406, "Kiran", 35000, "Proof reader");

        entitymanager.merge(employee1);
        entitymanager.merge(employee2);
        entitymanager.merge(employee3);
        entitymanager.merge(employee4);
        entitymanager.merge(employee5);
        entitymanager.merge(employee6);

        entitymanager.getTransaction().commit();

        //criteria

        //creearea criteria builder prin entitymanager
        CriteriaBuilder criteriaBuilder = entitymanager.getCriteriaBuilder();

        // CriteriaQuery instance is used to create a query object.
        // This query object’s attributes will be modified with the details of the query.
        CriteriaQuery<Object> criteriaQuery = criteriaBuilder.createQuery();

        //.from method is called to set the query root.
        Root<Employee> from = criteriaQuery.from(Employee.class);

        //select all records
        System.out.println("Select all records");

        //.select is called to set the result list type. + aici apelarea la from care este root-ul
        CriteriaQuery<Object> select = criteriaQuery.select(from);

        //TypedQuery<T> instance is used to prepare a query for execution
        // and specifying the type of the query result.
        TypedQuery<Object> typedQuery = entitymanager.createQuery(select);

        //getResultList method on the TypedQuery<T> object to execute a query.
        // This query returns a collection of entities, the result is stored in a List.
        List<Object> resultlist = typedQuery.getResultList();

        for(Object o:resultlist) {
            Employee e = (Employee)o;
            System.out.println("EID : " + e.getEid() + " Ename : " + e.getEname());
        }

        //Ordering the records
        System.out.println("Select all records by follow ordering");
        CriteriaQuery<Object> select1 = criteriaQuery.select(from);
        //ordoneaza ascendent dupa nume
        select1.orderBy(criteriaBuilder.asc(from.get("ename")));
        TypedQuery<Object> typedQuery1 = entitymanager.createQuery(select1);
        List<Object> resultlist1 = typedQuery1.getResultList();

        for(Object o:resultlist1){
            Employee e=(Employee)o;
            System.out.println("EID : " + e.getEid() + " Ename : " + e.getEname());
        }

        // + !! cu Meta Model == este True Type
        //exemplu cu where
        System.out.println("Select all records where salary is 30000");
        //echivalent la Query, mapped la prima CriteriaQuery creeata, prin entityManager,
        // select de form care e == cu clasa la care ne adresam
        CriteriaQuery<Object> select2 = criteriaQuery.select(from);
        //perfectam operatiunile de filtrare, criteriaBuilder fiind egal cu fiecare element,
        // utilizind from(la fel ca fiecare element)
        select2.where(criteriaBuilder.equal(from.get(ex8_Criteria.Employee_.salary),30000));
        //creem container pentru resultatul querry, utilizind CriteriaQuery creeata = select2
        TypedQuery<Object> typedQuery2 = entitymanager.createQuery(select2);
        //pre-luam rezultatul, in dependenta de obiectul pregatit pentru asta
        List<Object> resultlist2 = typedQuery2.getResultList();

        for(Object o:resultlist2){
            Employee e=(Employee)o;
            System.out.println("EID : " + e.getEid() + " Ename : " + e.getEname());
        }










        entitymanager.close( );
        emfactory.close( );
    }
}