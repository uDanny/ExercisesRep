package ex1_Basic;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;
//JPQL
public class ScalarandAggregateFunctions {
    public static void main( String[ ] args ) {

        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "JavaHelps" );
        EntityManager entitymanager = emfactory.createEntityManager();

        //Scalar function
        Query query = entitymanager.
                createQuery("Select UPPER(e.name) from Student e");
        List<String> list = query.getResultList();

        for(String e:list) {
            System.out.println("Studens id-s :"+e);
        }

        //Aggregate function
        Query query1 = entitymanager.createQuery("Select MAX(e.age) from Student e");
        int result =(Integer) query1.getSingleResult();
        System.out.println("Max Age :" + result);
    }
}
