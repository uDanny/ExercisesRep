package ex4_TablePerClass;

import javax.persistence.Entity;

@Entity
public class NonTeachingStaff2 extends Staff2 {
    private String areaexpertise;

    public NonTeachingStaff2(int sid, String sname, String areaexpertise ) {
        super( sid, sname );
        this.areaexpertise = areaexpertise;
    }

    public NonTeachingStaff2( ) {
        super( );
    }

    public String getAreaexpertise( ) {
        return areaexpertise;
    }

    public void setAreaexpertise( String areaexpertise ) {
        this.areaexpertise = areaexpertise;
    }
}