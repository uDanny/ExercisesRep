package ex4_TablePerClass;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
//TablePerClass
public class SaveClient2 {
    public static void main( String[ ] args ) {
        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "JavaHelps2" );
        EntityManager entitymanager = emfactory.createEntityManager( );
        entitymanager.getTransaction( ).begin( );

        //Teaching staff entity
        TeachingStaff2 ts1 = new TeachingStaff2(1,"Gopal","MSc MEd","Maths");
        TeachingStaff2 ts2 = new TeachingStaff2(2, "Manisha", "BSc BEd", "English");

        //Non-Teaching Staff2 entity
        NonTeachingStaff2 nts1 = new NonTeachingStaff2(3, "Satish", "Accounts");
        NonTeachingStaff2 nts2 = new NonTeachingStaff2(4, "Krishna", "Office Admin");

        //storing all entities
        entitymanager.merge(ts1);
        entitymanager.merge(ts2);
        entitymanager.merge(nts1);
        entitymanager.merge(nts2);

        entitymanager.getTransaction().commit();
        entitymanager.close();
        emfactory.close();
    }
}
