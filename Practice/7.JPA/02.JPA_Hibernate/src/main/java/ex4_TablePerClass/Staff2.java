package ex4_TablePerClass;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
@Inheritance( strategy = InheritanceType.TABLE_PER_CLASS )

public class Staff2 implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )

    private int sid;
    private String sname;

    public Staff2(int sid, String sname ) {
        super( );
        this.sid = sid;
        this.sname = sname;
    }

    public Staff2( ) {
        super( );
    }

    public int getSid( ) {
        return sid;
    }

    public void setSid( int sid ) {
        this.sid = sid;
    }

    public String getSname( ) {
        return sname;
    }

    public void setSname( String sname ) {
        this.sname = sname;
    }
}