package ex4_TablePerClass;

import javax.persistence.Entity;

@Entity
public class TeachingStaff2 extends Staff2 {
    private String qualification;
    private String subjectexpertise;

    public TeachingStaff2(int sid, String sname, String qualification, String subjectexpertise ) {
        super( sid, sname );
        this.qualification = qualification;
        this.subjectexpertise = subjectexpertise;
    }

    public TeachingStaff2( ) {
        super( );
    }

    public String getQualification( ){
        return qualification;
    }

    public void setQualification( String qualification ) {
        this.qualification = qualification;
    }

    public String getSubjectexpertise( ) {
        return subjectexpertise;
    }

    public void setSubjectexpertise( String subjectexpertise ){
        this.subjectexpertise = subjectexpertise;
    }
}