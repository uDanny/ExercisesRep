package ex2_SimpleTable;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
//SimpleTable
public class SaveClient {

    public static void main( String[ ] args ) {

        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("JavaHelps");
        EntityManager entitymanager = emfactory.createEntityManager();
        entitymanager.getTransaction().begin();

        //Teaching staff entity
        Staff ts1 = new TeachingStaff(1, "Gopal", "MSc MEd", "Maths");
        Staff ts2 = new TeachingStaff(2, "Manisha", "BSc BEd", "English");

        //Non-Teaching Staff2 entity
        Staff nts1 = new NonTeachingStaff(3, "Satish", "Accounts");
        Staff nts2 = new NonTeachingStaff(4, "Krishna", "Office Admin");

        //storing all entities
        entitymanager.merge(ts1);
        entitymanager.merge(ts2);
        entitymanager.merge(nts1);
        entitymanager.merge(nts2);

        entitymanager.getTransaction().commit();

        entitymanager.close();
        emfactory.close();
    }
}
