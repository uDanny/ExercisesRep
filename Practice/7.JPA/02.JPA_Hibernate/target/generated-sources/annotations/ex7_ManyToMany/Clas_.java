package ex7_ManyToMany;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Clas.class)
public abstract class Clas_ {

	public static volatile SingularAttribute<Clas, String> cname;
	public static volatile SetAttribute<Clas, Teacher> teacherSet;
	public static volatile SingularAttribute<Clas, Integer> cid;

}

