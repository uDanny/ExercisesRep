package ex7_ManyToMany;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Teacher.class)
public abstract class Teacher_ {

	public static volatile SingularAttribute<Teacher, String> subject;
	public static volatile SetAttribute<Teacher, Clas> clasSet;
	public static volatile SingularAttribute<Teacher, String> tname;
	public static volatile SingularAttribute<Teacher, Integer> tid;

}

