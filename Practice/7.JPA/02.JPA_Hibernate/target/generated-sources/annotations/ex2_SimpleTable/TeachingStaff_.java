package ex2_SimpleTable;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TeachingStaff.class)
public abstract class TeachingStaff_ extends ex2_SimpleTable.Staff_ {

	public static volatile SingularAttribute<TeachingStaff, String> qualification;
	public static volatile SingularAttribute<TeachingStaff, String> subjectexpertise;

}

