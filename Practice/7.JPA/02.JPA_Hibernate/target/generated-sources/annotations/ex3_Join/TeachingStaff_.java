package ex3_Join;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TeachingStaff.class)
public abstract class TeachingStaff_ extends ex3_Join.Staff_ {

	public static volatile SingularAttribute<TeachingStaff, String> qualification;
	public static volatile SingularAttribute<TeachingStaff, String> subjectexpertise;

}

