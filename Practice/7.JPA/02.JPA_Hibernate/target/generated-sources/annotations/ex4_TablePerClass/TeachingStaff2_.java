package ex4_TablePerClass;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TeachingStaff2.class)
public abstract class TeachingStaff2_ extends ex4_TablePerClass.Staff2_ {

	public static volatile SingularAttribute<TeachingStaff2, String> qualification;
	public static volatile SingularAttribute<TeachingStaff2, String> subjectexpertise;

}

