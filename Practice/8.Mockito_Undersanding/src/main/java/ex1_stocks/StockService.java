package ex1_stocks;

public interface StockService {
    public double getPrice(Stock stock);
}