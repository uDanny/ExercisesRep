package TypesOfInit;
/*
* - ocupa memorie incepind cu compilarea
* + mai repede se instantiaza( pu ca e deja instantiat
 */

public class Elvis_Eager {
    // Static member holds only one instance of the
    // SingletonExample class
    private static Elvis_Eager INSTANCE =
            new Elvis_Eager();

    // SingletonExample prevents any other class from instantiating
    private Elvis_Eager() {
    }

    // Providing Global point of access
    public static Elvis_Eager getSingletonInstance() {
        return INSTANCE;
    }

    private int nr;

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getNr() {
        return nr;
    }

    public void leaveTheBuilding() {
        //...
    }
}
