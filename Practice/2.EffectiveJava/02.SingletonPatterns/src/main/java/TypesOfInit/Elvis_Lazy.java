package TypesOfInit;

//Lazy init
/*
* + nu ocupa loc in memorie daca nu e initializat
* - nu poate multithreading, poate cadea la getInstance in acelasi timp
 */
public class Elvis_Lazy {
// Static member holds only one instance of the
// SingletonExample class
    private static Elvis_Lazy INSTANCE;

    // Providing Global point of access
    public static Elvis_Lazy getSingletonInstance() {
        if (null == INSTANCE) {
            INSTANCE = new Elvis_Lazy();
        }
        return INSTANCE;
    }

    private int nr;

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getNr() {
        return nr;
    }

    public void leaveTheBuilding() {
        //...
    }
}
