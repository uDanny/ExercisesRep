package TypesOfInit;

//Lazy init
/*
* + poate multithreading
* + nu strica performanta cum ar fi fost in cazul in care este syncronized toata metoda, astfel, se pierde doar la prima initializare
 */
public class Elvis_Lazy_DoubleCheckLocking {
// Static member holds only one instance of the
// SingletonExample class
    private static Elvis_Lazy_DoubleCheckLocking INSTANCE;

    // Providing Global point of access
    public static /*volatile*/ Elvis_Lazy_DoubleCheckLocking getSingletonInstance() {
        if (null == INSTANCE) { // verificam daca nu a fost instantiat
            synchronized (Elvis_Lazy_DoubleCheckLocking.class){ // blocul de sincronizare
                if (null == INSTANCE){ // mai verificam o data dupa syncronized
                    INSTANCE = new Elvis_Lazy_DoubleCheckLocking(); // creeam
                }
            }
        }
        return INSTANCE;
    }

    private int nr;

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getNr() {
        return nr;
    }

    public void leaveTheBuilding() {
        //...
    }
}
