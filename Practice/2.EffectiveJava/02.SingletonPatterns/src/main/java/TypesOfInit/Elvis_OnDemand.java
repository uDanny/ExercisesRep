package TypesOfInit;
/*
* + provide thread safety
* - arata ca si un builder
* - este considerat lazy
 */

public class Elvis_OnDemand {
    // Static member class member that holds only one instance of the
    // SingletonExample class
    private static class Elvis_OnDemand_Holder{
        public static Elvis_OnDemand INSTANCE =
                new Elvis_OnDemand();
    }
    // SingletonExample prevents any other class from instantiating
    private Elvis_OnDemand() {
    }

    // Providing Global point of access
    public static Elvis_OnDemand getSingletonInstance() {
        return Elvis_OnDemand_Holder.INSTANCE;
    }
    private int nr;

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getNr() {
        return nr;
    }

    public void leaveTheBuilding() {
        //...
    }
}
