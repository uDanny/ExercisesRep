package TypesOfInit;
/*
* + e la fel ca si Eager, dar are posibilitatea de a prinde exceptiile la instantiere
 */

import java.io.IOException;

public class Elvis_Block {

    private static Elvis_Block INSTANCE;
    static {
        try {
            INSTANCE = new Elvis_Block();
        } catch (Exception e) {
            throw new RuntimeException("Darn, an error occurred!", e);
        }
    }
    // SingletonExample prevents any other class from instantiating
    private Elvis_Block() {
    }

    // Providing Global point of access
    public static Elvis_Block getSingletonInstance() {
        return INSTANCE;
    }

    private int nr;

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getNr() {
        return nr;
    }

    public void leaveTheBuilding() {
        //...
    }
}
