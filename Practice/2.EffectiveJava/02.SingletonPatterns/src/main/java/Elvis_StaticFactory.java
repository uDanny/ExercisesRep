// Singleton with static factory
/*Singleton
* - la fel ca la PublicFinalField -> a privileged client can invoke the private constructor reflectively with the aid of the AccessibleObject.setAccessible method
* + defend against this attack, modify the constructor to make it throw an exception if it’s asked to create a second instance
* - mai complicat ca si constructie fata de FinalField
* + se pot face modificari la return ( de ex p/u fiecare thread cite unul )
* */
public class Elvis_StaticFactory {
    private final static Elvis_StaticFactory INSTANCE = new Elvis_StaticFactory();

    private Elvis_StaticFactory() {}
    public static Elvis_StaticFactory getInstance(){
        return INSTANCE;
    }

    private int nr;

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getNr() {
        return nr;
    }

    public void leaveTheBuilding() {
        //...
    }
}