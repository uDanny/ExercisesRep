import java.io.Serializable;

// Singleton with static factory + serializable
/*Singleton
* - INSANCE = transient
* - la deserealizare, luam o copie de al lui Elvis, care trebuie fixata prin:
* - apelarea metodei care transmite Elvis-ul original (INSTANCE), si lasa pentru garbage Collector sa-i stearga copia, aceasta metoda este apelata automat de Serialize
* */
public class Elvis_Serializable implements Serializable{
    //transient for non - serialize this field
    private transient final static Elvis_Serializable INSTANCE = new Elvis_Serializable();

    private Elvis_Serializable() {}
    public static Elvis_Serializable getInstance(){
        return INSTANCE;
    }

    private int nr;

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getNr() {
        return nr;
    }

    // readResolve method to preserve singleton property
    // This method is called immediately after an object of this class is deserialized.
    private Object readResolve() {
        // Instead of the object we’re on, return the class variable singleton
        // Return the one true Elvis and let the garbage collector
        // take care of the Elvis impersonator.
                return INSTANCE;
    }

    public void leaveTheBuilding() {
        //...
    }
}