// Singleton with public final field
/*Singleton
* + e clar ca e Singleton din din static final field
* - a privileged client can invoke the private constructor reflectively with the aid of the AccessibleObject.setAccessible method
* + defend against this attack, modify the constructor to make it throw an exception if it’s asked to create a second instance
* */
public class Elvis_PublicFinalField {
    public final static Elvis_PublicFinalField INSTANCE = new Elvis_PublicFinalField();
    private Elvis_PublicFinalField() {}

    private int nr;

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getNr() {
        return nr;
    }

    public void leaveTheBuilding() {
        //...
    }
}