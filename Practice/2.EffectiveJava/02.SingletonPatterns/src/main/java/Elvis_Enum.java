// Enum singleton - the preferred approach
/*Singleton
* + concis
* + nu are nevoie de constructor
* + suporta Serializarea, fara necesitatea adaugarii metodelor aditionale
* + garanteaza Unicitatea chiar si la serializare!
 */

public enum  Elvis_Enum {
    INSTANCE;

    private int nr;

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getNr() {
        return nr;
    }

}
