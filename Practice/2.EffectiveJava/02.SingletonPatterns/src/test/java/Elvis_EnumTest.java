import org.junit.Test;

import static org.junit.Assert.*;

public class Elvis_EnumTest {
    @Test
    public void testSingleton() throws Exception {
        Elvis_Enum elvis_enum = Elvis_Enum.INSTANCE;
        elvis_enum.setNr(20);
        Elvis_Enum elvis_enum2 = Elvis_Enum.INSTANCE;
        elvis_enum2.setNr(10);

        assert(elvis_enum.getNr() == elvis_enum2.getNr());
        assert(elvis_enum.hashCode() == elvis_enum2.hashCode());

    }
}