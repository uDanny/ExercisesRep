import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

public class Elvis_SerializableTest {
    @Test
    public void testSingleton() throws Exception {
        Elvis_Serializable elvis_serializable = Elvis_Serializable.getInstance();
        elvis_serializable.setNr(20);

        //serialize
        FileOutputStream fileOut = new FileOutputStream("elvis_serializable.ser");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(elvis_serializable);
        out.close();
        fileOut.close();

        //deserialize
        FileInputStream fileIn = new FileInputStream("elvis_serializable.ser");
        ObjectInputStream in = new ObjectInputStream(fileIn);
        Elvis_Serializable elvis_serializable2 = (Elvis_Serializable) in.readObject();
        in.close();
        fileIn.close();

        //check deserializare corecta
        assert (elvis_serializable.getNr() == elvis_serializable2.getNr());
        //check Singleton is right
        assert (elvis_serializable.hashCode() == elvis_serializable2.hashCode());

    }
}