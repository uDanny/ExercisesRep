import org.junit.Test;

import static org.junit.Assert.*;

public class Elvis_StaticFactoryTest {


    @Test
    public void testSingleton() throws Exception {
        Elvis_StaticFactory elvis_staticFactory = Elvis_StaticFactory.getInstance();
        elvis_staticFactory.setNr(20);
        Elvis_StaticFactory elvis_staticFactory2 = Elvis_StaticFactory.getInstance();
        elvis_staticFactory.setNr(30);


        assert(elvis_staticFactory==elvis_staticFactory2);
        assert(elvis_staticFactory.getNr()==elvis_staticFactory2.getNr());
    }
}