import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ElvisPublicFinalFieldTest {
    @Test
    public void testSingleton() throws Exception {
        Elvis_PublicFinalField elvisFinalField = Elvis_PublicFinalField.INSTANCE;
        elvisFinalField.setNr(20);
        Elvis_PublicFinalField elvisFinalField2 = Elvis_PublicFinalField.INSTANCE;
        elvisFinalField2.setNr(30);


        assert (elvisFinalField == elvisFinalField2);
        assert (elvisFinalField.getNr() == elvisFinalField2.getNr());

    }
}