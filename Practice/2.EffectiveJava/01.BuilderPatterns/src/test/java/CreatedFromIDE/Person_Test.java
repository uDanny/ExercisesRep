package CreatedFromIDE;

import org.junit.Test;

public class Person_Test {
    @Test
    public void testPatterns() throws Exception {
        Person person_builder = new PersonBuilder("aa","bb").createPerson_Builder_Temp();
        Person person_builder2 = new PersonBuilder("aa","bb").setAge(22).createPerson_Builder_Temp();
        Person person_builder3 = new PersonBuilder("aa","bb").setDiploma("aa").setGender('N').createPerson_Builder_Temp();


        assert (person_builder != null);
        assert (person_builder2 != null);
        assert (person_builder3 != null);

        assert (!person_builder3.getName().equals(null) && !person_builder3.getSurname().equals(null));
    }
}