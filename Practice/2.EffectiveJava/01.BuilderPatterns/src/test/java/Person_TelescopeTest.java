import org.junit.Test;

import static org.junit.Assert.*;

public class Person_TelescopeTest {
    @Test
    public void testPattern() throws Exception {
        //good cases
        Person_Telescope person = new Person_Telescope("aaa", "aaa");
        Person_Telescope person2 = new Person_Telescope("aaa", "aaa", 21);
        //bad cases
        Person_Telescope person3 = new Person_Telescope("aaa", "aaa", 0, 'M', null);

        assert(person != null);
        assert(person2 != null);
        assert(person3 != null);



    }
}