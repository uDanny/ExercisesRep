import org.junit.Test;

import static org.junit.Assert.*;

public class Person_JavaBeansTest {
    @Test
    public void testPattern() throws Exception {
        //good cases
        Person_JavaBeans person_javaBeans = new Person_JavaBeans();
        person_javaBeans.setName("aa");
        person_javaBeans.setSurname("bb");

        Person_JavaBeans person_javaBeans2 = new Person_JavaBeans();
        person_javaBeans2.setName("aa");
        person_javaBeans2.setSurname("bb");
        person_javaBeans2.setGender('F');

        //bad case:
        Person_JavaBeans person_javaBeans3 = new Person_JavaBeans();
        person_javaBeans3.setName("aa");
        person_javaBeans3.setGender('M');
        //Problem: atributul required nu e setat!

        assert (person_javaBeans != null);
        assert (person_javaBeans2 != null);
        assert (person_javaBeans3 != null);

        System.out.println(person_javaBeans3.getName());

        assert(!person_javaBeans3.getName().equals("REQUIRED") && !person_javaBeans3.getSurname().equals("REQUIRED"));
    }
}