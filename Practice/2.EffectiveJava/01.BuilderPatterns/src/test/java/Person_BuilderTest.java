import org.junit.Test;

import static org.junit.Assert.*;

public class Person_BuilderTest {
    @Test
    public void testPattern() throws Exception {
        //only good cases
        Person_Builder person_builder = new Person_Builder.Builder("aaa", "bbb").build();
        Person_Builder person_builder2 = new Person_Builder.Builder("aaa", "bbb").age(24).gender('M').diploma("oo").build();
        Person_Builder person_builder3 = new Person_Builder.Builder("aaa", "bbb").diploma("oo").build();

        assert (person_builder != null);
        assert (person_builder2 != null);
        assert (person_builder3 != null);

        assert (!person_builder3.getName().equals(null) && !person_builder3.getSurname().equals(null));

    }
}