public class Person_JavaBeans {
    /* JavaBeans Pattern
    * + nu avem probleme cu constructorele cind instantiem cu atributele 1, 2, 5
    * - exclude posibilitatea de immutable
    * - nu putem seta enforcing on validating
    * - avem de asigurat manual thread safety
    * */

    //required
    private  String name = "REQUIRED";
    private  String surname = "REQUIRED" ;

    //optional
    private  int age = 0;
    private  char gender = '\u0000';
    private  String diploma = null;

    public Person_JavaBeans() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getDiploma() {
        return diploma;
    }

    public void setDiploma(String diploma) {
        this.diploma = diploma;
    }
}
