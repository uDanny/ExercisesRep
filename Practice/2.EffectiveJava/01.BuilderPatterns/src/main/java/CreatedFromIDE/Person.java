package CreatedFromIDE;

public class Person {
    //required
    private final String name;
    private final String surname;

    //optional
    private final int age;
    private final char gender;
    private final String diploma;

    public Person(String name, String surname, int age, char gender, String diploma) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.gender = gender;
        this.diploma = diploma;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
}
