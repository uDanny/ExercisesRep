package CreatedFromIDE;

public class PersonBuilder {
    private final String name;
    private final String surname;
    private int age = 0;
    private char gender = '\u0000';
    private String diploma = null;

    public PersonBuilder(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public PersonBuilder setAge(int age) {
        this.age = age;
        return this;
    }

    public PersonBuilder setGender(char gender) {
        this.gender = gender;
        return this;
    }

    public PersonBuilder setDiploma(String diploma) {
        this.diploma = diploma;
        return this;
    }

    public Person createPerson_Builder_Temp() {
        return new Person(name, surname, age, gender, diploma);
    }
}