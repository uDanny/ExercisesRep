public class Person_Telescope {
    /* Telescope Pattern
    * + posibilitatea de final ( immutable )
    * + restrictia de instantierea cu atributele-parametri required
    * - scalarea ( multi parametri )
    * - imposibilitatea alegerea parametrilor in alta ordine( ex atributul 1, 2, 5), enforcing pentru folosirea constructorul general
    * */

    //required
    private final String name;
    private final String surname;

    //optional
    private final int age;
    private final char gender;
    private final String diploma;

    public Person_Telescope(String name, String surname) {
        this(name, surname, 0, '\u0000', null);
    }
    public Person_Telescope(String name, String surname, int age) {
        this(name, surname, age, '\u0000', null);
    }
    public Person_Telescope(String name, String surname, int age, char gender) {
        this(name, surname, age, gender, null);
    }


    public Person_Telescope(String name, String surname, int age, char gender, String diploma) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.gender = gender;
        this.diploma = diploma;
    }
}
