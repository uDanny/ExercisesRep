public class Person_Builder {
    /* Builder Pattern
    * + immutable
    * + restrictia de instantierea cu atributele-parametri required
    * + scalarea
    * + posibilitatea de a adauga oricare dintre parametri, in orice ordine
    * + usor de citit
    * + have posibility to add multiple varargs parameters
    * - performanta mai joasa ( din motivul instantierii a 2 obiecte )
    * - verbose
    * - mai complex de modificat
    * - safer then JavaBeans
    * */

    //required
    private final String name;
    private final String surname;

    //optional
    private final int age;
    private final char gender;
    private final String diploma;



    //static factory
    public static class Builder{
        //required
        private final String name;
        private final String surname;
        //optional
        private  int age = 0;
        private  char gender = '\u0000';
        private  String diploma = null;

        public Builder(String name, String surname) {
            this.name = name;
            this.surname = surname;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }
        public Builder gender(char gender) {
            this.gender = gender;
            return this;
        }
        public Builder diploma(String diploma) {
            this.diploma = diploma;
            return this;
        }

        public Person_Builder build(){
            return new Person_Builder(this);
        }
    }

    public Person_Builder(Builder builder) {
        this.name = builder.name;
        this.surname = builder.surname;
        this.age = builder.age;
        this.gender = builder.gender;
        this.diploma = builder.diploma;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
}
