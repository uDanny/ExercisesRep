import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarController implements AbstractController<Car> {
    public static String getAllQuerry = "SELECT * FROM car";
    Connection conn;

    public CarController(String url, String user, String pw) throws SQLException {
        conn = DriverManager.getConnection(url, user, pw);
    }

    public List<Car> getAll(String querry) {
        List<Car> carList = new ArrayList<Car>();
        try (
                PreparedStatement getAllStatement = conn.prepareStatement(querry);
                ResultSet rs = getAllStatement.executeQuery();
                ){
            while (rs.next()){
                Car car = new Car();

                car.setId(rs.getInt(1));
                car.setProducator(rs.getString(2));
                car.setPret(rs.getInt(3));
                car.setModel(rs.getString(4));
                car.setAn(rs.getInt(5));

                carList.add(car);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return carList;
    }




    public void closeConn(){
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
