import org.postgresql.jdbc2.optional.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public interface AbstractController <E>{ //E = obj, K = key
    List<E> getAll(String querry);

    void closeConn();

}
