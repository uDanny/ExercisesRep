import java.io.Serializable;

public class Car implements Serializable{
    int id;
    String producator;
    int pret;
    String model;
    int an;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProducator() {
        return producator;
    }

    public void setProducator(String producator) {
        this.producator = producator;
    }

    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getAn() {
        return an;
    }

    public void setAn(int an) {
        this.an = an;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", producator='" + producator + '\'' +
                ", pret=" + pret +
                ", model='" + model + '\'' +
                ", an=" + an +
                '}';
    }
}
