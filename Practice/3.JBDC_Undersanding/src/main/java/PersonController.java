import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PersonController implements AbstractController<Person> {
    public static String getAllQuerry = "SELECT * FROM person";
    public static String getInnerUnion = "SELECT id, name, surname, zip, age FROM person \n" +
            "inner join sales\n" +
            "on person.id = sales.idp";
    public static String getLeftJoin = "SELECT id, name, surname, zip, age FROM person \n" +
            "left join sales on person.id = sales.idp\n" +
            "where sales.idp is NULL";
    Connection conn;

    public PersonController(String url, String user, String pw) throws SQLException {
        conn = DriverManager.getConnection(url, user, pw);
    }

    @Override
    public List<Person> getAll(String querry) {
        List<Person> personList = new ArrayList<>();
        try (
                PreparedStatement getAllStatement = conn.prepareStatement(querry);
                ResultSet rs = getAllStatement.executeQuery();
        ){
            while (rs.next()){
                Person person = new Person();

                person.setId(rs.getInt(1));
                person.setName(rs.getString(2));
                person.setSurname(rs.getString(3));
                person.setZip(rs.getInt(4));
                person.setAge(rs.getInt(5));

                personList.add(person);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return personList;
    }



    public void closeConn(){
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
