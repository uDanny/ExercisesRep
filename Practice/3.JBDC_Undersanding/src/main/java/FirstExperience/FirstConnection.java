package FirstExperience;//STEP 1. Import required packages
import java.sql.*;

public class FirstConnection {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "org.postgresql.Driver";
    static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";

    //  Database credentials
    static final String USER = "postgres";
    static final String PASS = "1234qwer";



    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Connection conn = null;
        Statement stmt = null;

        //STEP 2: Register JDBC driver
        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("driver-ul nu a fost gasit");
        }

        //STEP 3: Open a connection
        System.out.println("Connecting to database...");
        conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
        System.out.println("Creating statement...");
        stmt = conn.createStatement();
        String sql;
        sql = "SELECT * FROM car";
        ResultSet rs = stmt.executeQuery(sql);

        //STEP 5: Extract data from result set
        displayCar(rs);


        System.out.println(" ------ prepared statement");


        ///Prepared statement
        PreparedStatement preparedStatement = null;
        preparedStatement = conn.prepareStatement(
                "SELECT * FROM car where id > ? and id < ?");
        preparedStatement.setInt(1, 3);
        preparedStatement.setInt(2, 5);
        ResultSet result2 = preparedStatement.executeQuery();
        displayCar(result2);


        System.out.println(" ------ tranzaction + rollback");

        conn.setAutoCommit(false);
        Statement stmt2 = null;
        ResultSet result3 = null;
        stmt2 = conn.createStatement();
        try {
            result3 = stmt2.executeQuery("SELECT * FROM car");
            conn.commit();
            System.out.println(conn.getAutoCommit());
            displayCar(result3);
        } catch (SQLException e) {
            conn.rollback();
        }


        System.out.println(" ------ meta-tags");

        DatabaseMetaData md = conn.getMetaData();
        ResultSet rs4 = md.getColumns(null, null, "car", null);
        while (rs4.next()) {
            System.out.println(rs4.getString(4));
        }

        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        ///
        preparedStatement.close();
        result2.close();
        //
        stmt2.close();
        result2.close();
        //
        rs4.close();


    }
    public static void displayCar(ResultSet rs) throws SQLException {
        while(rs.next()){
            //Retrieve by column name
            String producator = rs.getString("producator");
            int pret  = rs.getInt("pret");
            String model = rs.getString("model");
            int an  = rs.getInt("an");


            //Display values
            System.out.print("producator: " + producator);
            System.out.print(", pret: " + pret);
            System.out.print(", model: " + model);
            System.out.println(", an: " + an);
        }

    }

}
