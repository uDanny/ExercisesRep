import java.sql.SQLException;
import java.util.List;

public class Main {

    static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";

    //  Database credentials
    static final String USER = "postgres";
    static final String PASS = "1234qwer";


    public static void main(String[] args) {
        try {
            AbstractController daoExemplar = new CarController(DB_URL, USER, PASS);

            List<Car> carList = daoExemplar.getAll(CarController.getAllQuerry);
            //TODO: get querry
            daoExemplar.closeConn();

            System.out.println(carList.toString());

            //-------

            daoExemplar = new PersonController(DB_URL, USER, PASS);
            List<Person> personList = daoExemplar.getAll(PersonController.getAllQuerry);//TODO: get querry

            System.out.println(personList.toString());

            //--------
            daoExemplar = new PersonController(DB_URL, USER, PASS);
            List<Person> personList2 = daoExemplar.getAll(PersonController.getInnerUnion);//TODO: get querry

            System.out.println(personList2.toString());

            //--------
            daoExemplar = new PersonController(DB_URL, USER, PASS);
            List<Person> personList3 = daoExemplar.getAll(PersonController.getLeftJoin);//TODO: get querry

            System.out.println(personList3.toString());





        } catch (SQLException e) {
            e.printStackTrace();
        }




    }
}
