import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;



@ManagedBean(name = "helloWorld")
@SessionScoped
public class HelloBean implements Serializable {
    private static final long serialVersionUID = -6913972022251814607L;

    private String s1 = "Hello World from Bean!!";

    public String getS1() {
        System.out.println(s1);
        return s1;
    }

    public void setS1(String s1) {
        this.s1 = s1;
    }

}

